import { WolfService } from './../src/Services/WolfService';
import { GameStatus } from './../src/Core/Game/GameStatus';
import 'mocha';
import { PhaseKind } from './../src/Core/Game/Phase/PhaseKind';
import { CharacterMessage } from './../src/Core/Character/CharacterMessage';
import { MockedPhaseProvider } from './Mock/MockedPhaseProvider';
import { SimulationContext } from './../src/Services/SimulationContext';
import { PhaseManager } from './../src/Core/Game/Phase/PhaseManager';
import { Citizen } from '../src/Core/Character/Job/Citizen';
import { expect } from 'chai';
import { Wolf } from '../src/Core/Character/Job/Wolf';
import { MockedPhase } from './Mock/MockedPhase';
import { Character } from '../src/Core/Character/Character';
import { JobFactory } from '../src/Core/Character/Job/JobFactory';
import { Seer } from '../src/Core/Character/Job/Seer';
import { Witch } from '../src/Core/Character/Job/Witch';
import { Dictionnary } from '../src/Utils/Dictionnary';
import { Profil } from '../src/Core/Character/Profil';

describe('Phase',() =>
{
    it('Bob should be mayor when Alice votes for Bob',()=>{
        const context = SixPlayersContext('Angel');
        const provider = new MockedPhaseProvider(context);
        const phaseManager = new PhaseManager(context, provider);

        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = context.Characters.Get('Alice') 
        context.Characters.Get('Bob').ReceiveMessage(message);

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);
        expect(context.Characters.Get('Bob').GetVote()).to.equal(1);

        (phaseManager.CurrentPhase as MockedPhase).End();

        expect(context.Characters.Get('Bob').IsMayor()).to.equal(true);
    });

    it('citizen should win when wolves are dead',()=>{
        WolfService.Context= ThreePlayersContext();
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice');
        const kevin = WolfService.Context.Characters.Get('Kevin');

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);

        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = bob;
        alice.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        message.Text = 'fill-voting-smiley';
        message.Sender = bob;
        kevin.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.RedNight]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.PurpleNight]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        message.Text = 'fill-voting-smiley';
        message.Sender = bob;
        bob.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();

        expect(alice.GetStatus()).to.equal(GameStatus.Won);
        expect(bob.GetStatus()).to.equal(GameStatus.Lost);
        expect(kevin.GetStatus()).to.equal(GameStatus.Lost);
    });

    it('wolves should win when citizens are dead',()=>{
        WolfService.Context= ThreePlayersContext();
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice');
        const kevin = WolfService.Context.Characters.Get('Kevin');

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);

        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = bob;
        alice.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        message.Text = 'fill-voting-smiley';
        message.Sender = bob;
        alice.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();

        expect(alice.GetStatus()).to.equal(GameStatus.Lost);
        expect(bob.GetStatus()).to.equal(GameStatus.Won);
        expect(kevin.GetStatus()).to.equal(GameStatus.Won);
    });

    it('should kill alice when there is an equality and bob is mayor',()=>{
        WolfService.Context = SixPlayersContext('Angel');
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice');
        const kevin = WolfService.Context.Characters.Get('Kevin');

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);   

        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;
        bob.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);
     
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;
        bob.ReceiveMessage(message);

        message.Text = 'fill-voting-smiley';
        message.Sender = bob;
        alice.ReceiveMessage(message);

        expect(bob.GetVote()).to.equal(1);
        expect(alice.GetVote()).to.equal(1);
        expect(kevin.GetVote()).to.equal(0);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.RedNight]);

        expect(bob.IsDead()).to.equal(false);
        expect(alice.IsDead()).to.equal(true);
    });

    it('Bob should be winner when Alice votes against Bob for the first turn and Bob is Angel',()=>{
        WolfService.Context = SixPlayersContext('Angel');
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice');

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);   

        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;
        bob.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);
     
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;
        bob.ReceiveMessage(message);

        (phaseManager.CurrentPhase as MockedPhase).End();

        expect(bob.GetStatus()).to.equal(GameStatus.Won);
        expect(alice.GetStatus()).to.equal(GameStatus.Lost);
    });

    

    it('Bob should be able to vote when night and he is wolf',()=>{
        WolfService.Context = SixPlayersContext('Wolf');
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice') ;
        
        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);
        bob.ReceiveMessage(message);
        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        bob.IconsChanged.On((obj:any,icons:string[])=>{
            expect(icons.filter(i=>i === 'fill-voting-smiley').length).to.equal(1);
            bob.IconsChanged.Clear();
            message.Text = 'fill-voting-smiley';
            message.Sender = bob;
            alice.ReceiveMessage(message);
        });

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.RedNight]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.PurpleNight]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        expect(alice.IsDead()).to.equal(true);
        expect(alice.IsVisible()).to.equal(true);
    });


    it('Bob should be able to have magic ball when purple night and he is seer',()=>{
        WolfService.Context = SixPlayersContext('Seer');
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice') ;
        
        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);
        bob.ReceiveMessage(message);
        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.RedNight]);

        bob.IconsChanged.On((obj:any,icons:string[])=>{
            expect(icons.filter(i=>i === 'fill-magic-smiley').length).to.equal(1);
            bob.IconsChanged.Clear();
            bob.IconsChanged.On((obj:any,icons:string[])=>{
                expect(icons.filter(i=>i === 'fill-magic-smiley').length).to.equal(0);
            });
            message.Text = 'fill-magic-smiley';
            message.Sender = bob
            alice.ReceiveMessage(message);
        });

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.PurpleNight]);

        expect(alice.IsVisible()).to.equal(true);
    });


    it('Bob should be able to have potions when purple night and he is witch',()=>{
        WolfService.Context = SixPlayersContext('Witch');
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice') ;
        
        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);

        bob.ReceiveMessage(message);
        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.RedNight]);

        bob.IconsChanged.On((obj:any,icons:string[])=>{
            expect(icons.filter(i=>i === 'fill-potion-smiley').length).to.equal(1);
            bob.IconsChanged.Clear();

            bob.IconsChanged.On((obj:any,icons:string[])=>{
                expect(icons.filter(i=>i === 'fill-potion-smiley').length).to.equal(0);
            });

            message.Text = 'fill-potion-smiley';
            message.Sender = bob
            alice.ReceiveMessage(message);
        });

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.PurpleNight]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        expect(alice.IsDead()).to.equal(true);
        expect(alice.IsVisible()).to.equal(true);
        expect((bob.Job as Witch).HasKillingPotion).to.equal(false);
    });


    it('Bob should be able to save alice when david is wolf and bob is witch',()=>{
        WolfService.Context = SixPlayersContext('Witch');
        const provider = new MockedPhaseProvider(WolfService.Context);
        const phaseManager = new PhaseManager(WolfService.Context, provider);
        const bob = WolfService.Context.Characters.Get('Bob');
        const alice = WolfService.Context.Characters.Get('Alice') ;
        const kevin = WolfService.Context.Characters.Get('Kevin') ;
        
        const message = new CharacterMessage();
        message.Text = 'fill-voting-smiley';
        message.Sender = alice;

        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Mayor]);

        bob.ReceiveMessage(message);
        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        kevin.IconsChanged.On((obj:any,icons:string[])=>{
            expect(icons.filter(i=>i === 'fill-voting-smiley').length).to.equal(1);
            kevin.IconsChanged.Clear();

            message.Text = 'fill-voting-smiley';
            message.Sender = kevin;
            alice.ReceiveMessage(message);
        });

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.RedNight]);

        bob.IconsChanged.On((obj:any,icons:string[])=>{
            expect(icons.filter(i=>i === 'fill-savingPotion-smiley').length).to.equal(1);
            bob.IconsChanged.Clear();

            bob.IconsChanged.On((obj:any,icons:string[])=>{
                expect(icons.filter(i=>i === 'fill-savingPotion-smiley').length).to.equal(0);
            });

            message.Text = 'fill-savingPotion-smiley';
            message.Sender = bob
            alice.ReceiveMessage(message);
        });

        expect(alice.GetVote()).to.equal(1);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.PurpleNight]);

        (phaseManager.CurrentPhase as MockedPhase).End();
        expect(phaseManager.GetPhaseKind()).to.equal(PhaseKind[PhaseKind.Day]);

        expect(alice.IsDead()).to.equal(false);
        expect(alice.IsVisible()).to.equal(false);
        expect((bob.Job as Witch).HasSavingPotion).to.equal(false);
    });


	function Jobs(job: string): Character[] {
		return [
			Character.Create('Bob', emptyProfile, JobFactory.Generate(job)),
			Character.Create('Alice', emptyProfile, new Citizen()),
			Character.Create('Rachel', emptyProfile, new Seer()),
			Character.Create('Sofia', emptyProfile, new Wolf()),
			Character.Create('David', emptyProfile, new Witch()),
			Character.Create('Kevin', emptyProfile, new Wolf())
		];
    }

    var emptyProfile = new Profil('', '', '');
    
    function SixPlayersContext(job:string):SimulationContext{
        const context = new SimulationContext();
        context.Characters =  new Dictionnary<Character>();
        Jobs(job).forEach(c=>{
            context.Characters.Add(c.Name,c);
        });
        context.SetPlayer(context.Characters.Values()[0].Name);
        return context;
    }

    function ThreeJobs(): Character[] {
		return [
			Character.Create('Bob', emptyProfile, new Wolf()),
			Character.Create('Alice', emptyProfile, new Citizen()),
			Character.Create('Kevin', emptyProfile, new Wolf()),
		];
    }

    var emptyProfile = new Profil('', '', '');
    
    function ThreePlayersContext():SimulationContext{
        const context = new SimulationContext();
        context.Characters =  new Dictionnary<Character>();
        ThreeJobs().forEach(c=>{
            context.Characters.Add(c.Name,c);
        });
        context.SetPlayer(context.Characters.Values()[0].Name);
        return context;
    }

});