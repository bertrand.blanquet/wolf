import { IPhase } from "../../src/Core/Game/Phase/IPhase";
import { EnvKind } from "../../src/Core/Game/Phase/EnvKind";
import { PhaseKind } from "../../src/Core/Game/Phase/PhaseKind";
import { LiteEvent } from "../../src/Utils/EventHandler/LiteEvent";
import { IPhaseEvent } from "../../src/Core/Game/Phase/IPhaseEvent";
import { Character } from "../../src/Core/Character/Character";

export class MockedPhase implements IPhase{
    public Over: LiteEvent<boolean> = new LiteEvent<boolean>();
    public Elapsed: LiteEvent<number> = new LiteEvent<number>();
    public IsPaused: boolean;
    public Message: string;
    public Kind: EnvKind;

    private _event: IPhaseEvent;
	private _characters: Array<Character>;

	constructor(message: string, kind: EnvKind, characters: Array<Character>, event: IPhaseEvent, public PhaseKind:PhaseKind) {
		this.Message = message;
		this.Kind = kind;
		this._event = event;
		this._characters = characters;
	}

    Start(): void {
    }
    End():void{
        this._event.Do(this._characters);
        this._characters.forEach((c) => c.Clear());
        this.Over.Invoke(this,true);
    }

}