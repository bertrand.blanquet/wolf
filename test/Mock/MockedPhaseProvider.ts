import { PhaseKind } from './../../src/Core/Game/Phase/PhaseKind';
import { Dictionnary } from "../../src/Utils/Dictionnary";
import { Phase } from "../../src/Core/Game/Phase/Phase";
import { Character } from "../../src/Core/Character/Character";
import { IGameContext } from "../../src/Services/IGameContext";
import { IPhase } from "../../src/Core/Game/Phase/IPhase";
import { MockedPhase } from "./MockedPhase";
import { EnvKind } from "../../src/Core/Game/Phase/EnvKind";
import { ElectionPhaseEvent } from "../../src/Core/Game/Phase/ElectionPhaseEvent";
import { DayPhaseEvent } from "../../src/Core/Game/Phase/DayPhaseEvent";
import { RedPhaseEvent } from "../../src/Core/Game/Phase/RedPhaseEvent";
import { PurplePhaseEvent } from "../../src/Core/Game/Phase/PurplePhaseEvent";
import { IPhaseProvider } from "../../src/Core/Game/Phase/IPhaseProvider";

export class MockedPhaseProvider implements IPhaseProvider{
    private _phases:Dictionnary<()=>IPhase>;
    private _characters:Array<Character>;

    constructor(private _context:IGameContext){
        this._characters = _context.Characters.Values();
        this._phases = new Dictionnary<()=>Phase>();
        this._phases.Add(PhaseKind[PhaseKind.Mayor],()=> this.GetMayorPhase());
        this._phases.Add(PhaseKind[PhaseKind.Day],()=> this.GetDayPhase());
        this._phases.Add(PhaseKind[PhaseKind.RedNight],()=> this.GetRedPhase());
        this._phases.Add(PhaseKind[PhaseKind.PurpleNight],()=> this.GetPurplePhase());
    }

    public GetPhase(phase: string): IPhase {
        return this._phases.Get(phase)();
    }

    public GetFirstPhase(): IPhase {
        return this._phases.Values()[0]();
    }

    public GetNextPhase(formerPhase:string):IPhase{
        const keys = this._phases.Keys();
        let index = keys.indexOf(formerPhase);
        index =  (index+1) % keys.length;
        if(keys[index] === PhaseKind[PhaseKind.Mayor]){
            index =  (index+1) % keys.length;
        }
        return this._phases.Get(keys[index])();
    }

    private GetMayorPhase(): MockedPhase {
		return new MockedPhase('Pick a mayor', EnvKind.Daytime,this._characters, new ElectionPhaseEvent(),PhaseKind.Mayor);
    }
    
    private GetDayPhase(): MockedPhase {
		return new MockedPhase('Pick a suspect', EnvKind.Daytime,this._characters, new DayPhaseEvent(),PhaseKind.Day);
	}

	private GetRedPhase(): MockedPhase {
		return new MockedPhase('Wolfs make your move...', EnvKind.RedNight,this._characters, new RedPhaseEvent(),PhaseKind.RedNight);
	}

	private GetPurplePhase(): MockedPhase {
		return new MockedPhase('Witch and Seer make your moves...', EnvKind.PurpleNight,this._characters, new PurplePhaseEvent(),PhaseKind.PurpleNight);
	}
}