import 'mocha';
import { GameSettings } from '../src/Core/Game/GameSettings';
import { CharacterFactory } from '../src/Core/Character/CharacterFactory';
import { Citizen } from '../src/Core/Character/Job/Citizen';
import { expect } from 'chai';
import { Wolf } from '../src/Core/Character/Job/Wolf';

describe('Context',() =>
{
    it('modulo counter',()=>{
        expect(CharacterFactory.GetCount(2,4)).to.equal(1);
        expect(CharacterFactory.GetCount(3,4)).to.equal(1);
        expect(CharacterFactory.GetCount(4,4)).to.equal(1);
        expect(CharacterFactory.GetCount(5,4)).to.equal(2);
        expect(CharacterFactory.GetCount(6,4)).to.equal(2);
        expect(CharacterFactory.GetCount(9,4)).to.equal(3);
        expect(CharacterFactory.GetCount(13,4)).to.equal(4);
    });


    function GetSettings(names:string[]){
        let settings =new GameSettings();
        settings.Names = names;
        settings.Seer = true;
        settings.Witch =true;
        settings.Angel = true;
        return settings;
    }

    it('job counter 3 peoples',()=>{
        const characters = CharacterFactory.Generate(GetSettings(['Alice','Bob','John']));
        expect(characters.filter(c=>c.Job instanceof Wolf).length).to.equal(1);
        expect(characters.filter(c=>!(c.Job instanceof Citizen) && !(c.Job instanceof Wolf)).length).to.equal(1);
        expect(characters.filter(c=>c.Job instanceof Citizen).length).to.equal(1);
    });

    it('job counter 6 peoples',()=>{
        const characters = CharacterFactory.Generate(GetSettings(['Alice',
            'Bob',
            'John',
            'David',
            'Mick',
            'Ted']));
        expect(characters.filter(c=>c.Job instanceof Wolf).length).to.equal(2);
        expect(characters.filter(c=>c.Job instanceof Citizen).length).to.equal(2);
        expect(characters.filter(c=>!(c.Job instanceof Citizen) && !(c.Job instanceof Wolf)).length).to.equal(2);
    });
});