
import { h, render } from 'preact';
import HomeComponent from './Components/Home/HomeComponent';     
import GameComponent from './Components/Game/GameComponent';     
import CreatingHostComponent from './Components/Network/Creating/CreatingHostComponent';     
import HostingComponent from './Components/Network/Hosting/HostingComponent';     
import JoiningComponent from './Components/Network/Joining/JoiningComponent';     
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import Router from 'preact-router';
import './Style.css'
import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';

render(
    (<Router>
        <HomeComponent path="/Home" default/>
        <GameComponent path="/Game"/>
        <CreatingHostComponent path="/CreatingHost"/>
        <HostingComponent path="/Hosting/:RoomName/:playerName/:isAdmin"/>
        <JoiningComponent path="/Joining"/>
    </Router>)
    , document.querySelector('#app'));
