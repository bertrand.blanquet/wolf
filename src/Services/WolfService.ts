import { EnvKind } from './../Core/Game/Phase/EnvKind';
import { LiteEvent } from '../Utils/EventHandler/LiteEvent';
import { DroppingData } from '../Utils/DroppingData';
import { IGameContext } from './IGameContext';
import { Character } from '../Core/Character/Character';

export class WolfService{
    public static Dropped:LiteEvent<DroppingData> = new LiteEvent<DroppingData>();
    public static EnvKindChanged:LiteEvent<EnvKind> = new LiteEvent<EnvKind>();
    public static Context:IGameContext;

    public static HasSingleMaxCharacter(characters:Character[]):boolean{
        let max = WolfService.GetMax(characters);
        if(max === 0){
            return true;
        }
        let candidates = characters.filter(c=>c.GetVote() === max);
        if(candidates.length === 1){
            return true;
        }else{
            let mayorCandidate = candidates.filter(c=>c.HasMayorVote);
            return mayorCandidate.length === 1;
        }
    }

    public static HasNoVote(characters:Character[]){
        return WolfService.GetMax(characters) === 0;
    }

    public static GetMaxCharacter(characters:Character[]):Character{
        let max = WolfService.GetMax(characters);
        if(max === 0){
            throw console.error('should not be there');
        }
        let candidates = characters.filter(c=>c.GetVote() === max);
        if(candidates.length === 1){
            return candidates[0];
        }else{
            let mayorCandidate = candidates.filter(c=>c.HasMayorVote);
            if(mayorCandidate.length === 1){
                return mayorCandidate[0]
            }else{
                throw console.error('should not be there');
            }
        }
    }

    private static GetMax(characters: Character[]):number {
        let maxVoteCharacter = characters[0];
        characters.forEach(c => {
            if (maxVoteCharacter.GetVote() < c.GetVote()) {
                maxVoteCharacter = c;
            }
        });

        return maxVoteCharacter.GetVote();
    }
}