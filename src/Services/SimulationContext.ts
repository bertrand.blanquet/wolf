import { LiteEvent } from './../Utils/EventHandler/LiteEvent';
import { Simulator } from '../Core/Game/Phase/Simulator';
import { Dictionnary } from "../Utils/Dictionnary";
import { IGameContext } from './IGameContext';
import { Wolf } from '../Core/Character/Job/Wolf';
import { Character } from "../Core/Character/Character";
import { EnvKind } from "../Core/Game/Phase/EnvKind";

export class SimulationContext implements IGameContext{
    public CurrentPhase: string;
    public PhaseChanged: LiteEvent<string> = new LiteEvent<string>();
    public PauseChanged: LiteEvent<boolean> = new LiteEvent<boolean>();
    public DaytimeDuration: number=10;
    public RedNightDuration: number=10;
    public PurpleNightDuration: number=10;
    public Characters:Dictionnary<Character>;
    public Player:Character;
	private _simulator: Simulator = new Simulator();

    public GetAliveCharacters():Character[]{
        return this.Characters.Values().filter((character) => !character.IsDead());
    }

    public SetPlayer(name:string):void{
        this.Player = this.Characters.Get(name);
		this.Player.SetVisible(true);

		if (this.Player.Job instanceof Wolf) {
			this.Characters.Values().filter((c) => c.Job instanceof Wolf).forEach((c) => c.SetVisible(true));
		}
    }

    Destroy(): void {
    }

    Do(kind:EnvKind): void {
		this._simulator.Simulate(kind, this.GetAliveCharacters());
    }

}