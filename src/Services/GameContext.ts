import { MinCharacter } from './../Components/Network/Hosting/MinCharacter';
import { MinContext } from './../Components/Network/Hosting/MinContext';
import { LiteEvent } from './../Utils/EventHandler/LiteEvent';
import { MessageContent } from './MessageContent';
import { IGameContext } from './IGameContext';
import { Dictionnary } from './../Utils/Dictionnary';
import { NetworkSocket } from '../Utils/Network/NetworkSocket';
import { Character } from '../Core/Character/Character';
import { CharacterMessage } from '../Core/Character/CharacterMessage';
import { Wolf } from '../Core/Character/Job/Wolf';
import { EnvKind } from '../Core/Game/Phase/EnvKind';
import { PeerSocket } from '../Utils/Network/Peer/PeerSocket';
import { NetworkMessage } from '../Utils/Network/Message/NetworkMessage';
import { PacketKind } from '../Utils/Network/Message/PacketKind';
import { NetworkObserver } from '../Utils/EventHandler/NetworkObserver';

export class GameContext implements IGameContext {
	private _socket: NetworkSocket;

	public DaytimeDuration:number=30;
    public RedNightDuration:number=30;
    public PurpleNightDuration:number=30;

	public PauseChanged: LiteEvent<boolean> = new LiteEvent<boolean>();
	public PhaseChanged: LiteEvent<string> = new LiteEvent<string>();
	public Characters: Dictionnary<Character> = new Dictionnary<Character>();
	public CurrentPhase:string;
	public Player: Character;

	private _messageObserver:NetworkObserver;
	private _synchronisingObserver:NetworkObserver;
	private _pingObserver:NetworkObserver;

	constructor(){
		this._messageObserver = new NetworkObserver(PacketKind.Message,this.OnReceivedNetworkMessage.bind(this));
		this._pingObserver = new NetworkObserver(PacketKind.Ping,this.OnPingNetworkMessage.bind(this));
		this._synchronisingObserver = new NetworkObserver(PacketKind.Synchronised,this.OnSynchronising.bind(this));
	}

	public Setup(socket: NetworkSocket) {
		this._socket = socket;

		this.Characters.Values().forEach((character) => {
			character.ReceivedMessage.On(this.OnReceived.bind(this));
		});

		this._socket.OnReceived.On(this._messageObserver);
		this._socket.OnReceived.On(this._pingObserver);
		this._socket.OnReceived.On(this._synchronisingObserver);

		this._socket.OnPeerConnectionChanged.On((obj: any, peer: PeerSocket) => {
			const player = this.Characters.Get(peer.GetRecipient());
			player.SetConnection(peer.GetConnectionStatus().Kind);
		});

		this._socket.OnConnectedChanged.On((obj: any, isConnected: boolean)=>{
			if(isConnected && this.GetFirstName() === this.Player.Name){
				const packet = new NetworkMessage<MinContext>();
				packet.Kind = PacketKind.Synchronised;
				packet.Emitter = this.Player.Name;
				packet.Content = this.GetMinContext();
				packet.Recipient = PeerSocket.All();
				this._socket.Emit(packet);
				this.PhaseChanged.Invoke(this,this.CurrentPhase);
			}

			this.PauseChanged.Invoke(this,!isConnected);
		});
	}

	private GetMinContext():MinContext{
		let context = new MinContext();
		context.Phase = this.CurrentPhase;
		this.Characters.Values().forEach(character=>{
			let c = new MinCharacter();
			c.IsDead = character.IsDead();
			c.Name = character.Name;
			c.Profil = character.Profil;
			c.IsMayor = character.IsMayor();
			context.Characters.push(c)
		});
		return context;
	}

	private OnSynchronising(message:NetworkMessage<MinContext>):void{
		message.Content.Characters.forEach(c=>{
			const character = this.Characters.Get(c.Name);
			character.SetMayor(c.IsMayor);
			character.SetDead(c.IsDead);
			character.Clear();
		});
		this.PhaseChanged.Invoke(this,message.Content.Phase);
	} 

	private GetFirstName():string{
		return this.Characters.Values().sort((playerA, playerB) => (playerA.Name > playerB.Name ? 1 : -1))[0].Name;
	}

	private OnPingNetworkMessage(data: NetworkMessage<string>): void {
		if (this.Characters.Exist(data.Emitter)) {
			this.Characters.Get(data.Emitter).SetLatency(data.Content);
		}
	}

	private OnReceivedNetworkMessage(data: NetworkMessage<MessageContent>): void {
		var m = new CharacterMessage();
		m.Sender = this.Characters.Get(data.Emitter);
		m.Text = data.Content.Message;
		
		if(m.IsDropping())
		{
			m.Sender.DroppingVote.Invoke(this,m.Sender);
		}
		else
		{
			this.Characters.Get(data.Content.Target).ReceiveMessage(m);
		}
	}

	private OnReceived(obj: any, message: CharacterMessage): void {
		if (message.Sender.Name === this.Player.Name) {
			let target = obj as Character;
			if (this.HasPrivateContacts() 
				&& !message.IsAction()) {
				this.GetPrivateContacts().forEach((c) => {
					this.Emit(c.Name, target, message);
				});
			} else {
				this.Emit(PeerSocket.All(), target, message);
			}
		}
	}

	private Emit(recipient: string, target: Character, message: CharacterMessage) {
		var packet = new NetworkMessage<MessageContent>();
		packet.Kind = PacketKind.Message;
		packet.Recipient = recipient;
		packet.Emitter = this.Player.Name;

		packet.Content = new MessageContent();
		packet.Content.Target = target.Name;
		packet.Content.Message = message.Text;
		
		this._socket.Emit(packet);
	}

	public HasPrivateContacts(): boolean {
		return 0 < this.Characters.Values().filter((c) => c.IsCalled()).length;
	}

	public GetPrivateContacts(): Character[] {
		return this.Characters.Values().filter((c) => c.IsCalled());
	}

	public SetPlayer(name: string): void {
		this.Player = this.Characters.Get(name);
		this.Player.SetVisible(true);

		if (this.Player.Job instanceof Wolf) {
			this.Characters.Values().filter((c) => c.Job instanceof Wolf).forEach((c) => c.SetVisible(true));
		}
	}

	private OnDropping(sender: any, data: Character): void {
		var packet = new NetworkMessage<MessageContent>();
		packet.Kind = PacketKind.Message;
		packet.Recipient = PeerSocket.All();
		packet.Emitter = this.Player.Name;

		packet.Content = new MessageContent();
		packet.Content.Message = 'dropping';
		this._socket.Emit(packet);
	}

	Destroy(): void {
		this._socket.Stop();
		this.Player.DroppingVote.Clear();
	}

	Do(kind: EnvKind): void {
		this.Player.DroppingVote.On(this.OnDropping.bind(this))
	}
}
