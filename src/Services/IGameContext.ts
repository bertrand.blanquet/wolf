import { LiteEvent } from './../Utils/EventHandler/LiteEvent';
import { Dictionnary } from './../Utils/Dictionnary';
import { Character } from '../Core/Character/Character';
import { EnvKind } from '../Core/Game/Phase/EnvKind';

export interface IGameContext
{
    Characters:Dictionnary<Character>;
    Player:Character;
    
    DaytimeDuration:number;
    RedNightDuration:number;
    PurpleNightDuration:number;

    Do(kind:EnvKind):void;
    SetPlayer(name:string):void;
	Destroy():void;
    
    CurrentPhase:string;
    PhaseChanged: LiteEvent<string>;
    PauseChanged:LiteEvent<boolean>;
}