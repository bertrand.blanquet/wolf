import { GameSettings } from './../Core/Game/GameSettings';
import { CharacterFactory } from './../Core/Character/CharacterFactory';
import { GameContext } from './GameContext';

export class NetworkContextGenerator{
    
    public Generate(setting:GameSettings):GameContext{
        const context = new GameContext();
        CharacterFactory.Generate(setting).forEach(c=>{
            context.Characters.Add(c.Name,c);
        });
        return context;
    }
}