import { SimulationContext } from './SimulationContext';
import { Dictionnary } from '../Utils/Dictionnary';
import { CharacterFactory } from '../Core/Character/CharacterFactory';
import { Character } from '../Core/Character/Character';

export class SimulationContextGenerator{
    
    public Generate(job:string):SimulationContext{
        const context = new SimulationContext();
        context.Characters =  new Dictionnary<Character>();
        CharacterFactory.GenerateFromJob(job).forEach(c=>{
            context.Characters.Add(c.Name,c);
        });
        context.SetPlayer(context.Characters.Values()[0].Name);
        return context;
    }
}