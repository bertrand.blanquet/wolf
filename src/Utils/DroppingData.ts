import { CharacterMessage } from "../Core/Character/CharacterMessage";

export class DroppingData{
    Message:CharacterMessage;
    BoundingBox:DOMRect;
}