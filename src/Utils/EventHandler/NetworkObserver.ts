import { KindEventObserver } from "./KindEventObserver";
import { PacketKind } from "../Network/Message/PacketKind";
import { NetworkMessage } from "../Network/Message/NetworkMessage";

export class NetworkObserver extends KindEventObserver<PacketKind,NetworkMessage<any>>{
}