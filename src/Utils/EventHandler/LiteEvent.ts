
export class LiteEvent<T> {
    private _handlers: { (obj:any, data?: T): void; }[] = [];

    public On(handler: { (obj:any, data?: T): void }) : void {
        this._handlers.push(handler);
    }

    public Off(handler: { (obj:any, data?: T): void }) : void {
        this._handlers = this._handlers.filter(h => h !== handler);
    }

    public Clear(){
        this._handlers = [];
    }

    public Invoke(obj:any, data?: T) 
    {
        this._handlers.forEach(h => h(obj,data));
    }
}