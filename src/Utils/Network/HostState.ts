import { Dictionnary } from './../Dictionnary';
import { Player } from './Player'; 
import { GameSettings } from '../../Core/Game/GameSettings';

export class HostState{
    public RoomName:string;
    public IsAdmin:boolean;
    public Players:Dictionnary<Player>;
    public Settings:GameSettings;
    public Player:Player;
    public Message:string;
    public IaNumber:number;
}
