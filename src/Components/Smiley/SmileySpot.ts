export class SpotDiv{
    public Div:HTMLDivElement;
    public GetSpotPoint(): {top:number,left:number} {
		let top = 0,
            left = 0,
            element = this.Div as any;
		do {
			top += element.offsetTop || 0;
			left += element.offsetLeft || 0;
			element = element.offsetParent;
		} while (element);

		return {
			top: top,
			left: left
		};
	}
}