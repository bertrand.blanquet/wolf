import { h, Component } from 'preact';
import { WolfService } from '../../Services/WolfService';
import { DroppingData } from '../../Utils/DroppingData';
import { DragDiv } from './SmileyData';
import { SpotDiv } from './SmileySpot';
import { ResizeObserver } from 'resize-observer';
import { Character } from '../../Core/Character/Character';
import { EnvKind } from '../../Core/Game/Phase/EnvKind';
import { CharacterMessage } from '../../Core/Character/CharacterMessage';
import { Point } from '../Character/Point';
import { Dictionnary } from '../../Utils/Dictionnary';
const Draggable = require('Draggable');

export default class SmileyComponent extends Component<{ character: Character; env: EnvKind }, any> {
	private _drags: Array<DragDiv>;
	private _spots:Dictionnary<SpotDiv>;
	private _container: HTMLDivElement;
	private _smileyMenu: HTMLDivElement;
	private _character: Character;
	private _iconStyles: Array<string>;
	private _iconIndex: number = 0;
	private _observers: Array<ResizeObserver>;

	constructor() {
		super();
		this._observers = new Array<ResizeObserver>();
		this._spots = new Dictionnary<SpotDiv>();
		this._spots.Add('0',new SpotDiv());
		this._spots.Add('1',new SpotDiv());
		this._spots.Add('2',new SpotDiv());
		this._spots.Add('3',new SpotDiv());
		this._spots.Add('4',new SpotDiv());
		this._spots.Add('5',new SpotDiv());
		
		this._drags = [
			DragDiv.Create(0),
			DragDiv.Create(1),
			DragDiv.Create(2),
			DragDiv.Create(3),
			DragDiv.Create(4),
			DragDiv.Create(5)
		];
	}

	componentDidMount() {
		this.OnIconsChanged(this, this._character.Job.GetIcons(this.props.env));

		for (let i = 0; i < this._drags.length; i++) {
			this._drags[i].ClassName = this._iconStyles[i];
			this.InitDiv(this._drags[i]);
		}

		const observer = new ResizeObserver(() => {
			this._drags.forEach((drag) => {
				this.SetDragDefaultPosition(drag);
			});
		});
		observer.observe(this._container);
		this._observers.push(observer);
	}

	componentWillMount() {
		if (!this._character) {
			this._character = this.props.character;
			this._character.IconsChanged.On(this.OnIconsChanged.bind(this));
		}
	}

	componentDidUpdate(): void {}

	componentWillUpdate(): void {}

	private _currentPoint: Point;
	private _delta: number;
	private _isDragIn: boolean;

	componentWillUnmount() {
		this._observers.forEach((o) => {
			o.disconnect();
		});
		this._character.IconsChanged.Clear();
		this._drags.forEach((i) => {
			i.Div.remove();
			i.DragHandler.destroy();
		});
		this._spots.Values().forEach((spot) => {
			spot.Div.remove();
		});
		this._spots.Clear();
	}

	private InitDiv(drag: DragDiv): void {
		drag.Div.classList.add(drag.ClassName);

		const observer = new ResizeObserver(() => {
			drag.Div.style.left = `${this._spots.Get(drag.Index.toString()).GetSpotPoint().left}px`;
			drag.Div.style.top = `${this._spots.Get(drag.Index.toString()).GetSpotPoint().top}px`;
		});
		observer.observe(this._spots.Get(drag.Index.toString()).Div);
		this._observers.push(observer);

		drag.DragHandler = new Draggable(drag.Div);
		drag.DragHandler.options.onDragStart = (e: any, x: number, y: number, event: any) => {
			this._isDragIn = true;
			this._currentPoint = Point.Create(x, y);
			this._delta = 0;
			drag.Div.classList.remove('bouncing-up-animation');
			drag.IsDragged = true;
		};

		drag.DragHandler.options.onDrag = (e: HTMLDivElement, x: number, y: number, event: any) => {
			if (this._isDragIn) {
				const isContained = this.Intersect(e.getBoundingClientRect(), this._smileyMenu.getBoundingClientRect());
				const last = this._currentPoint;
				this._currentPoint = Point.Create(x, y);

				if (isContained) {
					this._delta += this._currentPoint.X - last.X;
					const width =e.offsetWidth; 
					if (width < Math.abs(this._delta)) {
						let remain =  Math.abs(Math.abs(this._delta)-width);
						this._delta = 0 <= this._delta ? remain:-remain;

						if (0 <= this._delta) {
							this.Decrement();
						} else {
							this.Increment();
						}
					}
				} else {
					this._isDragIn = false;
				}
			}
		};

		drag.DragHandler.options.onDragEnd = (e: HTMLDivElement, x: number, y: number, event: any) => {
			if(drag.IsDragged){
				drag.IsDragged = false;
				if (this._character.IsDead() && !this._character.IsMayor()) {
					return;
				}
				if(drag.ClassName === 'fill-voting-smiley'){
					this._character.DroppingVote.Invoke(this, this._character);
				}
				const droppingData = new DroppingData();
				droppingData.Message = new CharacterMessage();
				droppingData.Message.Text = e.classList[1];
				droppingData.Message.Sender = this._character;
				droppingData.BoundingBox = drag.Div.getBoundingClientRect();
				WolfService.Dropped.Invoke(this, droppingData);
			}
			this.SetDragDefaultPosition(drag);
			drag.Div.classList.add('bouncing-up-animation');
		};
		drag.Div.classList.add('bouncing-up-animation');
	}

	private SetDragDefaultPosition(drag: DragDiv) {
		drag.Div.style.left = `${this._spots.Get(drag.Index.toString()).GetSpotPoint().left}px`;
		drag.Div.style.top = `${this._spots.Get(drag.Index.toString()).GetSpotPoint().top}px`;
	}

	private GetStyle(): string {
		return `border-color:${this._character.Profil.Color};`;
	}

	private Decrement(): void {
		const icons = this.GetIcons();
		this.SwapSelectedToRightSpot();
		this.DecrementIndex(icons);
		this.UpdateSmiley(icons);
		this.SetDefaultAll();
		setTimeout(() => {
			this._drags.find(d=>d.Index === 0).Div.classList.add('bouncing-up-animation');
		}, 50);
	}

	private DecrementIndex(icons:string[]) {
		this._iconIndex = this._iconIndex - 1;
		if (this._iconIndex < 0) {
			this._iconIndex = icons.length - 1;
		}
	}

	private GetDrag():DragDiv{
		return this._drags.find(d=>d.IsDragged);
	}

	private SwapSelectedToRightSpot() {
		const selectedDrag = this.GetDrag();
		if (this._drags.length - 1 !== selectedDrag.Index) {
			const right = this._drags.find(d=>d.Index === selectedDrag.Index + 1);
			const rightIndex = right.Index;
			right.Index = selectedDrag.Index;
			selectedDrag.Index = rightIndex;
		}
	}

	private SwapSelectedToLeftSpot() {
		const selectedDrag = this.GetDrag();
		if (0 < selectedDrag.Index) {			
			const left = this._drags.find(d=>d.Index === selectedDrag.Index - 1);
			const leftIndex = left.Index;
			left.Index = selectedDrag.Index;
			selectedDrag.Index = leftIndex;
		}
	}

	private Increment(): void {
		const icons = this.GetIcons();
		this.SwapSelectedToLeftSpot();
		this.IncrementIndex(icons);
		this.UpdateSmiley(icons);
		this.SetDefaultAll();
		setTimeout(() => {
			this._drags.find(d=>d.Index === this._drags.length - 1).Div.classList.add('bouncing-up-animation');
		}, 50);
	}

	private SetDefaultAll() {
		this._drags.forEach((smiley) => {
			if (!smiley.IsDragged) {
				this.SetDragDefaultPosition(smiley);
			}
		});
	}

	private IncrementIndex(icons:string[]) {
		this._iconIndex = (this._iconIndex + 1) % icons.length;
	}

	private UpdateSmiley(icons:string[]) {
		let found = 0;
		for (let i = 0; i < this._drags.length; i++) {
			const drag = this._drags.find(d=>d.Index === i);
			if (!drag.IsDragged) {
				const currentIcon = (this._iconIndex + i - found) % icons.length;
				drag.Div.classList.remove(drag.ClassName);
				drag.ClassName = icons[currentIcon];
				drag.Div.classList.add(drag.ClassName);
				drag.Div.classList.remove('bouncing-up-animation');
			}else{
				found=1;
			}
		}
	}

	private GetIcons() {
		const selectedDrag = this.GetDrag();
		let ignoredIcon = '';
		if (selectedDrag) {
			ignoredIcon = selectedDrag.ClassName;
		}
		let currentImgs = this._iconStyles.filter((i) => i !== ignoredIcon);
		return currentImgs;
	}

	private CreateDivs(): any[] {
		const divs = new Array<any>();
		for (let i = 0; i < this._spots.Values().length; i++) {
			divs.push(
				<div
					class="inline-smiley"
					ref={(dom) => {
						this._drags[i].Div = dom;
					}}
				/>
			);
			divs.push(
				<div
					class="inline-smiley"
					ref={(dom) => {
						this._spots.Get(i.toString()).Div = dom;
					}}
				/>
			);			
		}

		return divs;
	}

	private OnIconsChanged(obj: any, imgs: string[]): void {
		if(this.GetDrag()){
			this.GetDrag().IsDragged =false;
		}
		this.SetDefaultAll();
		this._iconStyles = imgs;
		this._iconIndex = 0;
		this.UpdateSmiley(this._iconStyles);
		setTimeout(() => {
			this._drags.forEach((smiley) => {
				smiley.Div.classList.add('bouncing-up-animation');
			});
		}, 50);
	}

	private Intersect(r1: DOMRect, r2: DOMRect): boolean {
		return !(r2.left > r1.right || r2.right < r1.left || r2.top > r1.bottom || r2.bottom < r1.top);
	}

	render() {
		return (
			<div
				class="text-center"
				ref={(dom) => {
					this._container = dom;
				}}
			>
				<div
					class="smiley-menu"
					ref={(dom) => {
						this._smileyMenu = dom;
					}}
					style={this.GetStyle()}
				>
					{this.CreateDivs()}
				</div>
			</div>
		);
	}
}
