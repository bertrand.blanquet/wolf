export class DragDiv{
    public ClassName:string;
    public DragHandler:any;
    public Div:HTMLDivElement;
    public Index:number;
    public IsDragged:boolean=false;
    public static Create(index:number):DragDiv{
        const s = new DragDiv();
        s.Index = index;
        return s;
    }
}