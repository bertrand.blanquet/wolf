import { h, Component } from 'preact';
import { route } from 'preact-router';
import SmileyComponent from '../Smiley/SmileyComponent';
import CharacterComponent from '../Character/CharacterComponent';
import { WolfService } from '../../Services/WolfService';
import { IGameContext } from '../../Services/IGameContext';
import { EnvKind } from '../../Core/Game/Phase/EnvKind';
import { PhaseManager } from '../../Core/Game/Phase/PhaseManager';
import { PhaseProvider } from '../../Core/Game/Phase/PhaseProvider';
import { GameStatus } from '../../Core/Game/GameStatus';
let moment = require('moment');

export default class GameComponent extends Component<any, { message: string; duration: string }> {
	private _phaseManager: PhaseManager;
	private _context: IGameContext;
	private _skyDiv: HTMLDivElement;
	private _durationSpan: HTMLSpanElement;
	private _orbit: HTMLDivElement;
	private _aura: HTMLDivElement;
	private _formerEnv: EnvKind;

	constructor() {
		super();
		this._context = WolfService.Context;
		this._formerEnv = EnvKind.Daytime;
		this._phaseManager = new PhaseManager(this._context, new PhaseProvider(this._context));
		this._phaseManager.Elapsed.On(this.OnElapsed.bind(this));
		this._phaseManager.NextPhaseStarting.On(this.OnNextPhaseStarting.bind(this));
		this._phaseManager.GameOver.On((e: any, d: any) => {
			this._context.Destroy();
			this.setState({});
		});
		this._context.Do(EnvKind.Daytime);
	}

	private Back(e: any): void {
		this._context.Destroy();
		route('/Home', true);
	}

	private OnElapsed(obj: any, duration: number) {
		if(duration < 1){
			this._durationSpan.classList.remove('bouncing-up-down-animation');
			setTimeout(()=>{
				this._durationSpan.classList.add('bouncing-up-down-animation');
			},50);
		}
		this.setState({
			duration: `${moment.utc(duration * 1000).format('mm:ss')}`,
			message: this._phaseManager.GetMessage()
		});
	}

	private OnNextPhaseStarting(obj: any, nextEnv: EnvKind) {
		WolfService.EnvKindChanged.Invoke(this,nextEnv);
		this._context.Do(nextEnv);

		if (nextEnv === this._formerEnv) {
			return;
		}

		if (nextEnv === EnvKind.Daytime) {
			this.SetDay();
		} else if (nextEnv === EnvKind.PurpleNight) {
			this.SetPurpleNight();
		} else if (nextEnv === EnvKind.RedNight) {
			this.SetRedNight();
		}
		this._formerEnv = nextEnv;
	}

	componentDidMount() {
		this.SetDay();
	}

	private SetDay() {
		this._aura.className = '';
		this._skyDiv.className = '';
		this._orbit.className = '';
		this._skyDiv.classList.add('dayTime-animation');
		this._orbit.classList.add('fill-sun');
		this._orbit.classList.add('bouncing-up-animation');
		setTimeout(() => {
			this._orbit.classList.remove('bouncing-up-animation');
			this._orbit.classList.add('rotating');
		}, 500);
		this.setState({});
	}

	private SetRedNight(): void {
		this.SetNight();
		this.SetRedAura();
		this.setState({});
	}

	private SetPurpleNight(): void {
		this.SetNight();
		this.SetPurpleAura();
		this.setState({});
	}

	private SetNight(): void {
		this._skyDiv.className = '';
		this._orbit.className = '';
		this._skyDiv.classList.add('nightTime-animation');
		this._orbit.classList.add('fill-moon');
		this._orbit.classList.add('bouncing-up-animation');
		setTimeout(() => {
			this._orbit.classList.remove('bouncing-up-animation');
			this._orbit.classList.add('rotating');
		}, 500);
	}

	private SetRedAura() {
		this._aura.classList.add('fill-red-aura');
		setTimeout(() => {
			this._aura.classList.add('opacity-changing');
		}, 500);
	}

	private SetPurpleAura() {
		this._aura.classList.add('fill-purple-aura');
		setTimeout(() => {
			this._aura.classList.add('opacity-changing');
		}, 500);
	}

	componentWillUnmount() {
		this._context.Destroy();
		this._phaseManager.Clear();
	}

	render() {
		return (
			<div
				ref={(dom) => {
					this._skyDiv = dom;
				}}
			>
				<div class="text-center" style="width=100%;height:10vh; position: relative;">
					<div class="child">
						<div ref={(dom) => {
							this._durationSpan = dom;
						}}class="duration-container">
							{this.GetTopMessage()}
						</div>
					</div>
					<div
						style="height:100%;width:10vh;display: inline-block;"
						ref={(dom) => {
							this._orbit = dom;
						}}
					>
						<div
							style="height:100%;width:10vh;display: inline-block;"
							ref={(dom) => {
								this._aura = dom;
							}}
						/>
					</div>
				</div>
				<div style="position: fixed;top: 0;right: 0;">
					<button type="button" class="btn btn-primary btn-sm btn-danger" onClick={(e) => this.Back(e)}>
						Back
					</button>
				</div>
				<div style="height:80vh;">
					<div class="container">
						<div class="row justify-content-center">
							{this._context.Characters.Values().map((character) => (
								<div class="col-auto">
									<CharacterComponent character={character} />
								</div>
							))}
						</div>
					</div>
				</div>
				{this.GetEndMessage()}
				<div style="height:10vh;">
					<SmileyComponent character={this._context.Player} env={this._phaseManager.GetCurrentEnv()} />
				</div>
			</div>
		);
	}

	private GetTopMessage(){
		if(this.state.message){
			return `${this.state.message} ${this.state.duration}`
		}else{
			return 'Loading';
		}
	}

	private GetEndMessage() {
		if (this._context.Player.GetStatus() === GameStatus.Won) {
			return (
				<div class="absolute-center-middle smiley-menu" style="text-align:center;">
					<div class="fill-victory" style="width:20vh;height:20vh" />
					<button type="button" class="btn btn-danger" style="margin:10px" onClick={(e) => this.Back(e)}>
						back
					</button>
				</div>
			);
		} else if (this._context.Player.GetStatus() === GameStatus.Lost) {
			return (
				<div class="absolute-center-middle smiley-menu" style="text-align:center;">
					<div class="fill-rip" style="width:20vh;height:20vh" />
					<button type="button" class="btn btn-danger" style="margin:10px" onClick={(e) => this.Back(e)}>
						back
					</button>
				</div>
			);
		}
		return '';
	}
}
