import { MinCharacter } from './MinCharacter';

export class MinContext{
    public DaytimeDuration:number=30;
    public RedNightDuration:number=30;
    public PurpleNightDuration:number=30;
    public Characters: Array<MinCharacter>=new Array<MinCharacter>();
    public Phase:string;
}