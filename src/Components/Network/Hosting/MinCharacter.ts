import { Profil } from './../../../Core/Character/Profil';
export class MinCharacter{
    public Name:string;
    public Job:string;
    public Profil:Profil;
    public IsMayor:boolean;
    public IsDead:boolean;
}