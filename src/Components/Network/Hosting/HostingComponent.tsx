import { Component, h } from 'preact';
import { route } from 'preact-router';
import linkState from 'linkstate';
import * as toastr from 'toastr';
import { HostState } from '../../../Utils/Network/HostState';
import { Player } from '../../../Utils/Network/Player';
import { NetworkSocket } from '../../../Utils/Network/NetworkSocket';
import { NetworkService } from '../../../Services/NetworkService';
import { Dictionnary } from '../../../Utils/Dictionnary';
import { NetworkContextGenerator } from '../../../Services/NetworkContextGenerator';
import { WolfService } from '../../../Services/WolfService';
import { GameContext } from '../../../Services/GameContext';
import { Character } from '../../../Core/Character/Character';
import { Profil } from '../../../Core/Character/Profil';
import { JobFactory } from '../../../Core/Character/Job/JobFactory';
import PlayersComponent from './Players/PlayersComponent';
import { IconProvider } from '../../../IconProvider';
import OptionComponent from '../Option/OptionComponent';
import { GameSettings } from '../../../Core/Game/GameSettings';
import { MinContext } from './MinContext';
import { PeerSocket } from '../../../Utils/Network/Peer/PeerSocket';
import { MinCharacter } from './MinCharacter';
import { NetworkObserver } from '../../../Utils/EventHandler/NetworkObserver';
import { PacketKind } from '../../../Utils/Network/Message/PacketKind';
import { NetworkMessage } from '../../../Utils/Network/Message/NetworkMessage';

export default class HostingComponent extends Component<any, HostState> {
	private _socket: NetworkSocket;
	private _isFirstRender = true;
	private _hasSettings: boolean = false;

	private _readyObserver: NetworkObserver;
	private _toastObserver: NetworkObserver;
	private _contextObserver: NetworkObserver;
	private _playersObserver: NetworkObserver;
	private _pingObserver: NetworkObserver;
	private _onPeerConnectionChanged:any;
	constructor(props: any) {
		super(props);
		this._readyObserver = new NetworkObserver(PacketKind.Ready, this.OnPlayerReadyChanged.bind(this));
		this._toastObserver = new NetworkObserver(PacketKind.Toast, this.OnToastReceived.bind(this));
		this._contextObserver = new NetworkObserver(PacketKind.Context, this.OnStarted.bind(this));
		this._playersObserver = new NetworkObserver(PacketKind.Players, this.OnPlayersChanged.bind(this));
		this._pingObserver = new NetworkObserver(PacketKind.Ping, this.OnPingChanged.bind(this));
		toastr.options.closeDuration = 10000;

		const p = new Player(props.playerName);
		p.IsReady = false;
		const dictionnary = new Dictionnary<Player>();
		dictionnary.Add(p.Name, p);

		this.setState({
			RoomName: props.RoomName,
			Players: dictionnary,
			IsAdmin: props.isAdmin.toLowerCase() == 'true' ? true : false,
			Player: p,
			Message: '',
			IaNumber: 0,
			Settings: new GameSettings()
		});

		this._socket = new NetworkSocket(props.playerName, props.RoomName,this.state.IsAdmin);

		this._onPeerConnectionChanged = this.PeerConnectionChanged.bind(this);
		this._socket.OnPeerConnectionChanged.On(this._onPeerConnectionChanged);
		this._socket.OnReceived.On(this._toastObserver);
		this._socket.OnReceived.On(this._readyObserver);
		this._socket.OnReceived.On(this._contextObserver);
		this._socket.OnReceived.On(this._playersObserver);
		this._socket.OnReceived.On(this._pingObserver);

		NetworkService.Dispatcher.Init(!this.state.IsAdmin);
	}

	componentDidMount() {
		this._isFirstRender = false;
	}

	componentWillUnmount() {
		this._socket.OnReceived.Off(this._toastObserver);
		this._socket.OnReceived.Off(this._readyObserver);
		this._socket.OnReceived.Off(this._contextObserver);
		this._socket.OnReceived.Off(this._playersObserver);
		this._socket.OnPeerConnectionChanged.Off(this._onPeerConnectionChanged);
	}

	render() {
		return (
			<div>
				<div class="generalContainer absolute-center-middle">
					<div class="title-container">
						{IconProvider.GetIcon(this._isFirstRender, 'fab fa-wolf-pack-battalion')} {this.state.RoomName}
					</div>
					{this.GetButtons()}
					<PlayersComponent NetworkHandler={this._socket} HostState={this.state} />
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<button
								class="btn btn-dark"
								type="button"
								id="button-addon1"
								onClick={() => this.SendToast()}
							>
								{IconProvider.GetIcon(this._isFirstRender, 'fas fa-comment')}
							</button>
						</div>
						<input
							type="text"
							class="form-control"
							id="toastMessageBox"
							value={this.state.Message}
							onInput={linkState(this, 'Message')}
							aria-label="Example text with button addon"
							aria-describedby="button-addon1"
						/>
					</div>
				</div>
				{this.GetSettings()}
			</div>
		);
	}
	GetSettings() {
		if (this._hasSettings) {
			return (
				<div class="optionContainer absolute-center-middle">
					<div class="title-container">Settings</div>
					<OptionComponent Data={this.state.Settings} Update={this.Update.bind(this)} />
				</div>
			);
		} else {
			return '';
		}
	}

	private PeerConnectionChanged(obj: any, peer: PeerSocket):void {
		if(peer){
			const player = this.state.Players.Get(peer.GetRecipient());
			if(player){
				player.Connection = peer.GetConnectionStatus();
				this.setState({});
			}
		}
	}
	private Update(gameSetting: GameSettings): void {
		gameSetting.DaytimeDuration = +gameSetting.DaytimeDuration;
		gameSetting.PurpleNightDuration = +gameSetting.PurpleNightDuration;
		gameSetting.RedNightDuration = +gameSetting.RedNightDuration;
		this._hasSettings = false;
		this.setState({
			Settings: gameSetting
		});
	}

	private GetButtons() {
		return (
			<div class="bottom-space-out">
				<button type="button" class="btn btn-primary btn-sm btn-light left" onClick={() => this.Back()}>
					{IconProvider.GetIcon(this._isFirstRender, 'fas fa-undo-alt')} CLOSE
				</button>
				{this.state.IsAdmin ? (
					<button
						type="button"
						class="btn btn-danger btn-sm right very-small-left-margin"
						onClick={() => this.Start()}
					>
						{IconProvider.GetIcon(this._isFirstRender, 'far fa-play-circle')} START
					</button>
				) : (
					''
				)}
				<button
					type="button"
					class="btn btn-dark btn-sm right very-small-left-margin"
					onClick={() => this.ChangeReady()}
				>
					{IconProvider.GetIcon(this._isFirstRender, 'fas fa-toggle-on')} {this.GetButtonLabel()}
				</button>
				{this.state.IsAdmin ? (
					<button
						type="button"
						class="btn btn-dark btn-sm right very-small-left-margin"
						onClick={() => {
							this._hasSettings = true;
							this.setState({});
						}}
					>
						{IconProvider.GetIcon(this._isFirstRender, 'fas fa-cog')} SETTINGS
					</button>
				) : (
					''
				)}
			</div>
		);
	}

	private GetButtonLabel() {
		if (this.state.Player.IsReady) {
			return 'READY';
		}
		return 'NOT READY';
	}

	private OnPlayerReadyChanged(data: NetworkMessage<boolean>): void {
		if(this.state.Players.Exist(data.Emitter)){
			this.state.Players.Get(data.Emitter).IsReady = data.Content;
			this.setState({});
		}
	}

	private OnToastReceived(message: NetworkMessage<string>): void {
		if (message) {
			toastr['success'](message.Content, `> ${message.Emitter}`, { iconClass: 'toast-gray' });
			document.getElementById('toastMessageBox').focus();
		}
	}

	private OnPlayersChanged(message: NetworkMessage<string[]>): void {
		message.Content.forEach((playerName) => {
			if (!this.state.Players.Exist(playerName)) {
				this.state.Players.Add(playerName, new Player(playerName));
			}
		});

		this.state.Players.Keys().filter((p) => message.Content.indexOf(p) === -1).forEach((c) => {
			this.state.Players.Remove(c);
		});

		this.setState({});
	}

	private OnPingChanged(message: NetworkMessage<string>): void {
		if (this.state.Players.Exist(message.Emitter)) {
			this.state.Players.Get(message.Emitter).Latency = message.Content;
			this.setState({});
		}
	}

	private ChangeReady(): void {
		const player = this.state.Player;
		player.IsReady = !player.IsReady;
		this.setState({});
		const message = new NetworkMessage<boolean>();
		message.Emitter = this.state.Player.Name;
		message.Content = player.IsReady;
		message.Kind = PacketKind.Ready;
		message.Recipient = PeerSocket.All();
		this._socket.Emit(message);
	}

	private SendToast(): void {
		let message = new NetworkMessage<string>();
		message.Emitter = this.state.Player.Name;
		message.Recipient = PeerSocket.All();
		message.Content = this.state.Message;
		message.Kind = PacketKind.Toast;
		this.setState({
			Message: ''
		});

		toastr['success'](message.Content, `> ${message.Emitter}`, { iconClass: 'toast-white' });
		this._socket.Emit(message);
		document.getElementById('toastMessageBox').focus();
	}

	private Start(): void {
		if (this.IsEveryoneReady()) {
			this.state.Settings.Names = this.state.Players.Values().map((p) => p.Name);
			const context = new NetworkContextGenerator().Generate(this.state.Settings);
			const content = new MinContext();
			context.Characters.Values().forEach((c) => {
				const minCharacter = new MinCharacter();
				minCharacter.Name = c.Name;
				minCharacter.Job = c.Job.Title;
				minCharacter.Profil = c.Profil;
				content.Characters.push(minCharacter);
			});
			content.PurpleNightDuration = this.state.Settings.PurpleNightDuration;
			content.RedNightDuration = this.state.Settings.RedNightDuration;
			content.DaytimeDuration = this.state.Settings.DaytimeDuration;

			const message = new NetworkMessage<MinContext>();
			message.Emitter = this.state.Player.Name;
			message.Recipient = PeerSocket.All();
			message.Kind = PacketKind.Context;
			message.Content = content;

			this._socket.Emit(message);
			this.HideRoom();
			this.OnStarted(message);
		}
	}

	private HideRoom():void{
		const message = new NetworkMessage<any>();
		message.Emitter = this.state.Player.Name;
		message.Recipient = PeerSocket.Server();
		message.Kind = PacketKind.Hide;
		this._socket.Emit(message)
	}

	private OnStarted(data: NetworkMessage<MinContext>): void {
		const context = new GameContext();
		context.Characters = new Dictionnary<Character>();
		data.Content.Characters.forEach((character) => {
			context.RedNightDuration = data.Content.RedNightDuration;
			context.PurpleNightDuration = data.Content.PurpleNightDuration;
			context.DaytimeDuration = data.Content.DaytimeDuration;
			context.Characters.Add(
				character.Name,
				Character.Create(
					character.Name,
					new Profil(character.Profil.Picture, character.Profil.Color, character.Profil.Background),
					JobFactory.Generate(character.Job)
				)
			);
			context.Characters
				.Get(character.Name)
				.SetConnection(this.state.Players.Get(character.Name).Connection.Kind);
			context.Characters.Get(character.Name).SetLatency(this.state.Players.Get(character.Name).Latency);
		});
		context.Setup(this._socket);
		context.SetPlayer(this.state.Player.Name);
		WolfService.Context = context;
		route('/Game', true);
	}

	private IsEveryoneReady(): boolean {
		let players = this.state.Players.Values();
		for (let i = 0; i < players.length; i++) {
			if (!players[i].IsReady) {
				return false;
			}
		}
		return true;
	}

	private Back(): void {
		this._socket.Stop();
		route('/Home', true);
	}
}
