export class Point{
    public X:number;
    public Y:number;

    public static Create(x:number,y:number):Point{
        const p = new Point();
        p.X = x;
        p.Y = y;
        return p;
    }
}