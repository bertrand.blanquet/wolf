import { h, Component } from 'preact';
import { Point } from './Point';
import { WolfService } from '../../Services/WolfService';
import { DroppingData } from '../../Utils/DroppingData';
import { Wolf } from '../../Core/Character/Job/Wolf';
import { Character } from '../../Core/Character/Character';
import { CharacterMessage } from '../../Core/Character/CharacterMessage';
import { CharacterSmileyData } from '../../Core/Character/CharacterSmileyData';
import { EnvKind } from '../../Core/Game/Phase/EnvKind';
import { Witch } from '../../Core/Character/Job/Witch';
import { Seer } from '../../Core/Character/Job/Seer';
import { ConnectionKind } from '../../Utils/Network/ConnectionKind';

export default class CharacterComponent extends Component<
	{ character: Character },
	{ messages: Array<CharacterMessage>; EnvKind: EnvKind }
> {
	private spots: number = 8;
	private _character: Character;
	private _characterDiv: HTMLDivElement;
	private _smileys: Array<CharacterSmileyData>;
	private positions: Array<Point>;
	public X: number;
	public Y: number;

	constructor() {
		super();
		this.setState({
			messages: new Array<CharacterMessage>()
		});
		this._smileys = new Array<CharacterSmileyData>();
		this.positions = new Array<Point>();
		WolfService.Dropped.On(this.OnDropped.bind(this));
		WolfService.EnvKindChanged.On(this.OnEnvKindChanged.bind(this));
	}

	private OnDropped(source: any, data: DroppingData): void {
		if (
			this.Intersect(data.BoundingBox, this._characterDiv.getBoundingClientRect()) &&
			this._smileys.length < this.spots
		) {
			this._character.ReceiveMessage(data.Message);
		}
	}

	private OnEnvKindChanged(source: any, data: EnvKind): void {
		this.setState({
			EnvKind: data
		});
	}

	private OnReceivedMessage(obj: any, message: CharacterMessage) {
		if (this.IsNotWolf(message) || this.IsNotWitch(message) || this.IsNotSeer(message)) {
			return;
		}

		this.GeneratePositions();

		const messages = this.state.messages;
		messages.push(message);

		const lastMessage = this.state.messages[this.state.messages.length - 1];
		this.CreateSmiley(lastMessage.Text, lastMessage.Sender.Profil.Background);
		this.UpdateSmileyPositions();
		setTimeout(() => {
			const smiley = this._smileys.filter((s) => !s.IsDeleting)[0];
			smiley.IsDeleting = true;
			smiley.Div.classList.remove('bouncing-up-animation');
			smiley.Div.classList.add('bouncing-down-animation');
			smiley.BackgroundDiv.classList.remove('bouncing-up-animation');
			smiley.BackgroundDiv.classList.add('bouncing-down-animation');
			setTimeout(() => {
				const messages = this.state.messages;
				messages.shift();
				this.RemoveSmiley();
				this.UpdateSmileyPositions();
				this.setState({
					messages: messages
				});
			}, 300);
		}, 3000);
		this.setState({
			messages: messages
		});
	}

	private IsNotWolf(message: CharacterMessage) {
		return (
			message.IsVote() &&
			this.state.EnvKind === EnvKind.RedNight &&
			!(WolfService.Context.Player.Job instanceof Wolf)
		);
	}

	private IsNotWitch(message: CharacterMessage) {
		return (
			(message.IsGreenPotion() || message.IsRedPotion() || message.IsTarget()) &&
			!(WolfService.Context.Player.Job instanceof Witch)
		);
	}

	private IsNotSeer(message: CharacterMessage) {
		return message.IsMagicBall() && !(WolfService.Context.Player.Job instanceof Seer);
	}

	componentWillMount() {
		if (!this._character) {
			this._character = this.props.character;
			this._character.ReceivedMessage.On(this.OnReceivedMessage.bind(this));
			this._character.Changed.On((obj: any, e: string) => {
				if (e === 'IsMayor') {
					this._characterDiv.classList.add('bouncing-up-down-animation');
				}
				this.setState({});
			});
		}
	}

	componentDidMount() {}

	private GeneratePositions(): void {
		const distance = 360 / this.spots;
		for (var i = 3; i <= this.spots + 2; ++i) {
			this.positions.push(this.GeneratePosition(distance, i));
		}
	}

	private GeneratePosition(distance: number, i: number): Point {
		const x = Math.cos(distance * i * (Math.PI / 180)) * Math.round(window.innerHeight / 100) * 7;
		const y = Math.sin(distance * i * (Math.PI / 180)) * Math.round(window.innerHeight / 100) * 7;
		return Point.Create(x, y);
	}

	RemoveSmiley(): void {
		const deletingSmiley = this._smileys[0];
		this._smileys = this._smileys.filter((s) => s !== deletingSmiley);
		deletingSmiley.Div.remove();
		deletingSmiley.BackgroundDiv.remove();
	}

	private CreateSmiley(className: string, characterColor: string): void {
		const smileyData = new CharacterSmileyData(document.createElement('div'), document.createElement('div'));
		this._smileys.push(smileyData);

		this.AddDiv(smileyData.BackgroundDiv, characterColor);
		this.AddDiv(smileyData.Div, className);
	}

	private AddDiv(div: HTMLDivElement, classname: string): void {
		div.classList.add('smileyDiv');
		div.classList.add(classname);
		div.classList.add('bouncing-up-animation');
		this._characterDiv.appendChild(div);
	}

	private UpdateSmileyPositions(): void {
		if (0 < this.state.messages.length) {
			const offsetToParentCenter = Math.round(+this._characterDiv.offsetWidth / 2);
			const offsetToChildCenter = 20;
			const totalOffset = offsetToParentCenter - offsetToChildCenter;
			for (let i = 0; i < this._smileys.length; i++) {
				this._smileys[i].Div.style.left = `${this.positions[i].X + totalOffset}px`;
				this._smileys[i].Div.style.top = `${this.positions[i].Y + totalOffset}px`;
				this._smileys[i].BackgroundDiv.style.left = `${this.positions[i].X + totalOffset}px`;
				this._smileys[i].BackgroundDiv.style.top = `${this.positions[i].Y + totalOffset}px`;
			}
		}
	}

	componentWillUnmount() {
		this._character.ReceivedMessage.Clear();
		this._character.Changed.Clear();
		WolfService.Dropped.Clear();
	}

	private Intersect(r1: DOMRect, r2: DOMRect): boolean {
		return !(r2.left > r1.right || r2.right < r1.left || r2.top > r1.bottom || r2.bottom < r1.top);
	}

	render() {
		return (
			<div style="width:-webkit-max-content; margin-left:10px;margin-right:10px;">
				<div
					onClick={(e) => this.SetCalled()}
					class={this.GetClassName()}
					ref={(dom) => {
						this._characterDiv = dom;
					}}
				>
					{this.GetMayor()}
				</div>
				<div class="character-title" style={this.GetColorStyle()}>
					{this.GetCalled()} <span style={this.GetVoteStyle()}>{this.GetVote()}</span> {this._character.Name}
				</div>
				{this.GetJob(this._character)}
			</div>
		);
	}

	private GetJob(character: Character) {
		if (this._character.Name === WolfService.Context.Player.Name || this._character.GetLatency() === '') {
			return <div class="job-title">{this._character.Job.Title}</div>;
		} else {
			return (
				<div class="job-title">
					{' '}
					<span style={this.GetLatencyStyle(this._character)}>{this._character.GetLatency()}</span>{' '}
					{this._character.IsVisible() ? this._character.Job.Title : '-'}
				</div>
			);
		}
	}

	private GetVote() {
		if (this.state.EnvKind === EnvKind.RedNight && !(WolfService.Context.Player.Job instanceof Wolf)) {
			return '?';
		} else {
			return this._character.GetVote();
		}
	}

	private SetCalled(): void {
		this._character.SetCalled(!this._character.IsCalled());
	}

	private GetCalled(): any {
		return this._character.IsCalled() ? (
			<span class="fill-phone" style="padding:10px;">
				{' '}
			</span>
		) : (
			''
		);
	}

	private GetMayor(): any {
		return this._character.IsMayor() ? <div class="character fill-mayor" /> : '';
	}

	public GetVoteStyle() {
		return `background-color:#1E1E1E; padding-left: 2px; padding-right: 2px;color:white;`;
	}

	public GetLatencyStyle(character: Character) {
		let color = '#FCC00D';
		if (character.GetConnection() === ConnectionKind.Ok) {
			color = '#27A745';
		}
		if (character.GetConnection()  === ConnectionKind.Nok) {
			color = '#DD3545';
		}

		return `background-color:${color}; padding-left: 2px; padding-right: 2px;color:white;`;
	}

	public GetColorStyle() {
		return `border-color:${this._character.Profil.Color};`;
	}

	private GetClassName(): string {
		return `character ${this._character.Profil.Picture}`;
	}
}
