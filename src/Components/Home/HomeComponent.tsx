import { h, Component } from 'preact';
import { route } from 'preact-router';
import { SimulationContextGenerator } from '../../Services/SimulationContextGenerator';
import { WolfService } from '../../Services/WolfService';
import { IconProvider } from '../../IconProvider';

export default class HomeComponent extends Component<any, any> {
	private _isFirstRender = true;
	constructor() {
		super();
	}

	private Simultate(job: string): void {
		const context = new SimulationContextGenerator().Generate(job);
		WolfService.Context = context;
		route(`/Game`, true);
	}

	private ToHost(e: any): void {
		route('/CreatingHost', true);
	}

	private ToJoin(e: any): void {
		route('/Joining', true);
	}

	componentDidMount() {
		this._isFirstRender = false;
	}

	componentWillUnmount() {}

	render() {
		return (
			<div class="generalContainer absolute-center-middle">
				<div class="title-container">
					{IconProvider.GetIcon(this._isFirstRender, 'fab fa-wolf-pack-battalion')} Wolf
				</div>
				<div class="btn-group-vertical btn-block">
					<button
						type="button"
						class="btn btn-danger dropdown-toggle dropdown-toggle-split"
						data-toggle="dropdown"
						aria-haspopup="true"
						aria-expanded="false"
					>
						{IconProvider.GetIcon(this._isFirstRender, 'fas fa-bars')} Simulate{' '}
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<div class="dropdown-menu">
						<a class="dropdown-item" onClick={() => this.Simultate('Citizen')}>
							Citizen
						</a>
						<a class="dropdown-item" onClick={() => this.Simultate('Angel')}>
							Angel
						</a>
						<a class="dropdown-item" onClick={() => this.Simultate('Wolf')}>
							Wolf
						</a>
						<a class="dropdown-item" onClick={() => this.Simultate('Seer')}>
							Seer
						</a>
						<a class="dropdown-item" onClick={() => this.Simultate('Witch')}>
							Witch
						</a>
					</div>
					<div class="btn-group btn-primary-blue btn-block" role="group">
						<button
							id="btnGroupDrop1"
							type="button"
							class="btn btn-primary-blue btn-block dropdown-toggle"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="false"
						>
							{IconProvider.GetIcon(this._isFirstRender, 'fas fa-network-wired')} Multiplayers
						</button>
						<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
							<a class="dropdown-item" onClick={this.ToHost}>
								Host
							</a>
							<a class="dropdown-item" onClick={this.ToJoin}>
								Join
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
