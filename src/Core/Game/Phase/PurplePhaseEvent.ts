import { IPhaseEvent } from "./IPhaseEvent";
import { Character } from "../../Character/Character";

export class PurplePhaseEvent implements IPhaseEvent
{
    public Do(characters:Character[]):boolean{
        const targets = characters.filter(c=>c.IsTarget() === true);
        
        if(0 < targets.length){
            targets.forEach(t=>{
                t.SetDead(true);
            });
        }

        return true;
    }
}