import { EnvKind } from './EnvKind';
import { Character } from '../../Character/Character';
import { CharacterMessage } from '../../Character/CharacterMessage';


export class Simulator{
    public Simulate(env:EnvKind,characters:Character[]):void{
        if(env === EnvKind.Daytime){
            let i = 1;
            characters.forEach(c=>{
                const index = Math.floor(Math.random() * characters.length);
                const victim = characters[index];
                const m = new CharacterMessage();
                m.Text = 'fill-voting-smiley';
                m.Sender = c;
                i += 1;
                setTimeout(()=>{
                    victim.ReceiveMessage(m);
                },i*400);
            });
        }
        else if(env === EnvKind.RedNight){
            const victims = characters.filter(c=>c.Job.Title !== 'Wolf');
            if(0 < victims.length){
                const index = Math.floor(Math.random() * victims.length);
                const victim = victims[index];
                characters.filter(c=>c.Job.Title === 'Wolf').forEach(c=>{
                    const m = new CharacterMessage();
                    m.Text = 'fill-voting-smiley';
                    m.Sender = c;
                    victim.ReceiveMessage(m);
                });
            }
        }
    }
}