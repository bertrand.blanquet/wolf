import { LiteEvent } from "../../../Utils/EventHandler/LiteEvent";
import { PhaseKind } from "./PhaseKind";
import { EnvKind } from "./EnvKind";

export interface IPhase{
    Over: LiteEvent<boolean>;
	Elapsed: LiteEvent<number>;
	IsPaused:boolean;
    Message: string;
    PhaseKind:PhaseKind;
    Kind: EnvKind;

    Start(): void ;
}