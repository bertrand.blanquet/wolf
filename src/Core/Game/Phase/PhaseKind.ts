export enum PhaseKind{
    Mayor,
    Day,
    RedNight,
    PurpleNight,
}