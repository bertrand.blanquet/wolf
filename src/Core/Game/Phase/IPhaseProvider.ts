import { IPhase } from './IPhase';
export interface IPhaseProvider{
     GetFirstPhase(): IPhase;
     GetPhase(phase:string):IPhase;
     GetNextPhase(formerPhase:string):IPhase;
}