import { PhaseKind } from './PhaseKind';
import { Character } from './../../Character/Character';
import { AngelPhaseChecker } from './Checker/AngelPhaseChecker';
import { NoWolfPhaseChecker } from './Checker/NoWolfPhaseChecker';
import { WolfPhaseChecker } from './Checker/WolfPhaseChecker';
import { PhaseCheckManager } from './Checker/PhaseCheckManager';
import { EnvKind } from "./EnvKind";
import { IPhaseProvider } from "./IPhaseProvider";
import { LiteEvent } from '../../../Utils/EventHandler/LiteEvent';
import { IGameContext } from '../../../Services/IGameContext';
import { IPhase } from './IPhase';

export class PhaseManager
{
    private _checker:PhaseCheckManager; 
	public NextPhaseStarting: LiteEvent<EnvKind> = new LiteEvent<EnvKind>();
	public Elapsed: LiteEvent<number> = new LiteEvent<number>();
    public GameOver: LiteEvent<object> = new LiteEvent<object>();
    private _characters:Array<Character>;
    public CurrentPhase: IPhase;
    private _phaseCount:number=-1;

    constructor(private _context:IGameContext, private _phaseProvider:IPhaseProvider){
        this._characters = _context.Characters.Values();

        this._checker = new PhaseCheckManager([
            new WolfPhaseChecker(),
            new NoWolfPhaseChecker(),
            new AngelPhaseChecker()
        ]);
        this._context.PauseChanged.On((obj:any,isPaused:boolean)=>{
            if(this.CurrentPhase){
                this.CurrentPhase.IsPaused = isPaused;
            }
        });

        this._context.PhaseChanged.On((obj:any,phase:string)=>{
            if(this.CurrentPhase){
                this.CurrentPhase.Over.Clear();
                this.CurrentPhase.Elapsed.Clear();
            }
            this.CurrentPhase = this._phaseProvider.GetPhase(phase);
            this.StartCurrentPhase()
        });

        this.CurrentPhase = this._phaseProvider.GetFirstPhase();
        this.StartCurrentPhase()
    }

    private OnOver(obj:any,data:boolean):void{
        this.CurrentPhase.Over.Clear();
        this.CurrentPhase.Elapsed.Clear();
        this._phaseCount=1+this._phaseCount;

        if(this._checker.IsOver(this._characters,this._phaseCount)){
            this.GameOver.Invoke(this,this);
            return;
        }

        this.CurrentPhase = this._phaseProvider.GetNextPhase(PhaseKind[this.CurrentPhase.PhaseKind]);
        this.StartCurrentPhase();
    }

    private StartCurrentPhase() {
        this._context.CurrentPhase = PhaseKind[this.CurrentPhase.PhaseKind];
        this.CurrentPhase.Over.On(this.OnOver.bind(this));
        this.CurrentPhase.Elapsed.On((e: any, data: number) => {
            this.Elapsed.Invoke(this, data);
        });
        this.NextPhaseStarting.Invoke(this, this.CurrentPhase.Kind);
        this._characters.forEach(c => {
            c.UpdateIcons(this.CurrentPhase.Kind);
        });
        this.CurrentPhase.Start();
    }

    public GetMessage():string{
        return this.CurrentPhase.IsPaused ? 'synchronizing' : this.CurrentPhase.Message;
    }

    public GetCurrentEnv():EnvKind{
        return this.CurrentPhase.Kind;
    }

    public GetPhaseKind():string{
        return PhaseKind[this.CurrentPhase.PhaseKind];
    }

    public Clear():void{
        if(this.CurrentPhase){
            this.CurrentPhase.Over.Clear();
            this.CurrentPhase.Elapsed.Clear();
        }
        this.Elapsed.Clear();
		this.NextPhaseStarting.Clear();
		this.GameOver.Clear();
    }
}