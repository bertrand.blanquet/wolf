import { WolfService } from '../../../Services/WolfService';
import { IPhaseEvent } from './IPhaseEvent';
import { Character } from '../../Character/Character';

export class ElectionPhaseEvent implements IPhaseEvent {
	public Do(characters: Character[]): boolean {
		if (WolfService.HasNoVote(characters)) {
			return false;
		}

		if (WolfService.HasSingleMaxCharacter(characters)) {
			let c = WolfService.GetMaxCharacter(characters);
            c.SetMayor(true);
            return true;
		}

		return false;
	}
}
