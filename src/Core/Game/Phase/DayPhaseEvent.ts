import { IPhaseEvent } from './IPhaseEvent';
import { WolfService } from '../../../Services/WolfService';
import { Character } from '../../Character/Character';

export class DayPhaseEvent implements IPhaseEvent
{
    public Do(characters:Character[]):boolean{
        if(WolfService.HasNoVote(characters)){
            return true;
        }

        if(WolfService.HasSingleMaxCharacter(characters)){
            let c = WolfService.GetMaxCharacter(characters);
            if(0 < c.GetVote()){
                c.SetDead(true);
            }
            return true;
        }
        else
        {
            return false;
        }

    }
}