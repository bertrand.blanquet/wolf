import { Witch } from '../../Character/Job/Witch';
import { IPhaseEvent } from './IPhaseEvent';
import { WolfService } from '../../../Services/WolfService';
import { Character } from '../../Character/Character';
import { CharacterMessage } from '../../Character/CharacterMessage';

export class RedPhaseEvent implements IPhaseEvent
{
    public Do(characters:Character[]):boolean{
        if(WolfService.HasNoVote(characters)){
            return true;
        }

        if(WolfService.HasSingleMaxCharacter(characters)){
            let target = WolfService.GetMaxCharacter(characters);
            target.SetTarget(true);
            let p = WolfService.Context.Player;
            
            if(!p.IsDead() && p.Job instanceof Witch){
                let message = new CharacterMessage();
                message.Text = 'fill-target';
                target.ReceiveMessage(message);
            }
            return true;
        }
        return false;
    }
}