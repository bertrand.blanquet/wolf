import { PurplePhaseEvent } from './PurplePhaseEvent';
import { RedPhaseEvent } from './RedPhaseEvent';
import { DayPhaseEvent } from './DayPhaseEvent';
import { ElectionPhaseEvent } from './ElectionPhaseEvent';
import { Dictionnary } from "../../../Utils/Dictionnary";
import { Phase } from "./Phase";
import { PhaseKind } from "./PhaseKind";
import { EnvKind } from "./EnvKind";
import { IGameContext } from "../../../Services/IGameContext";
import { Character } from "../../Character/Character";
import { IPhaseProvider } from './IPhaseProvider';
import { IPhase } from './IPhase';

export class PhaseProvider implements IPhaseProvider{
    private _phases:Dictionnary<()=>Phase>;
    private _characters:Array<Character>;

    constructor(private _context:IGameContext){
        this._characters = _context.Characters.Values();
        this._phases = new Dictionnary<()=>Phase>();
        this._phases.Add(PhaseKind[PhaseKind.Mayor],()=> this.GetMayorPhase());
        this._phases.Add(PhaseKind[PhaseKind.Day],()=> this.GetDayPhase());
        this._phases.Add(PhaseKind[PhaseKind.RedNight],()=> this.GetRedPhase());
        this._phases.Add(PhaseKind[PhaseKind.PurpleNight],()=> this.GetPurplePhase());
    }

    public GetPhase(phase: string): IPhase {
        return this._phases.Get(phase)();
    }

    public GetFirstPhase(): import("./IPhase").IPhase {
        return this._phases.Values()[0]();
    }

    public GetNextPhase(formerPhase:string):IPhase{
        const keys = this._phases.Keys();
        let index = keys.indexOf(formerPhase);
        index =  (index+1) % keys.length;
        if(keys[index] === PhaseKind[PhaseKind.Mayor]){
            index =  (index+1) % keys.length;
        }
        return this._phases.Get(keys[index])();
    }

    private GetMayorPhase(): Phase {
		return new Phase('Pick a mayor', EnvKind.Daytime,this._context.DaytimeDuration, this._characters, new ElectionPhaseEvent(),PhaseKind.Mayor);
    }
    
    private GetDayPhase(): Phase {
		return new Phase('Pick a suspect', EnvKind.Daytime,this._context.DaytimeDuration, this._characters, new DayPhaseEvent(),PhaseKind.Day);
	}

	private GetRedPhase(): Phase {
		return new Phase('Wolfs make your move...', EnvKind.RedNight,this._context.RedNightDuration, this._characters, new RedPhaseEvent(),PhaseKind.RedNight);
	}

	private GetPurplePhase(): Phase {
		return new Phase('Witch and Seer make your moves...', EnvKind.PurpleNight,this._context.PurpleNightDuration, this._characters, new PurplePhaseEvent(),PhaseKind.PurpleNight);
	}

}