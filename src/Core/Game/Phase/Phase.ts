import { PhaseKind } from './PhaseKind';
import { EnvKind } from './EnvKind';
import { IPhase } from './IPhase';
import { IPhaseEvent } from './IPhaseEvent';
import { LiteEvent } from '../../../Utils/EventHandler/LiteEvent';
import { Character } from '../../Character/Character';

export class Phase implements IPhase{
	public Over: LiteEvent<boolean> = new LiteEvent<boolean>();
	public Elapsed: LiteEvent<number> = new LiteEvent<number>();
	public IsPaused:boolean;
	public Message: string;
	public Kind: EnvKind;

	private Duration: number;
	private CurrentDuration: number;
	private _event: IPhaseEvent;
	private _characters: Array<Character>;
	private _idleTime: number;
	private _timer: any;

	constructor(message: string, kind: EnvKind, duration: number, characters: Array<Character>, event: IPhaseEvent, public PhaseKind:PhaseKind) {
		this.Message = message;
		this.Kind = kind;
		this.Duration = duration;
		this._event = event;
		this.CurrentDuration = duration;
		this._characters = characters;
	}

	public Start(): void {
		this._characters.forEach((c) => {
			c.Changed.On(this.OnVoteDropping.bind(this));
		});

		this._timer = setInterval(() => {

			if(!this.IsPaused){
				this.CurrentDuration = this.CurrentDuration - 1;
			}
			
			if (this.CurrentDuration < 0) {
				if (this._event.Do(this._characters)) {
					clearInterval(this._timer);
					this.Elapsed.Clear();
					this.OnOver();
				} else {
					this.CurrentDuration = this.Duration;
					this.Elapsed.Invoke(this, this.CurrentDuration);
				}
			} else {
				this.Elapsed.Invoke(this, this.CurrentDuration);
			}
		}, 1000);
	}

	private OnVoteDropping(obj: any, character: Character): void {
		this._idleTime = 0;
	}

	private OnOver(): void {
		this._characters.forEach((c) => c.Clear());
		this.Over.Invoke(this, true);
	}
}

// if (5 < this.CurrentDuration) {
// 	this._idleTime += 1;
// 	if (5 < this._idleTime) {
// 		this.CurrentDuration = 5
// 	}
// }
