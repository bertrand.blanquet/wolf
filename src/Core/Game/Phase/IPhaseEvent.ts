import { Character } from "../../Character/Character";

export interface IPhaseEvent{
    Do(characters:Character[]):boolean;
}