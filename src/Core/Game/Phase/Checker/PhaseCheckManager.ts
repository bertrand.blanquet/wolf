import { PhaseChecker } from "./PhaseChecker";
import { Character } from "../../../Character/Character";

export class PhaseCheckManager extends PhaseChecker{

    constructor(private _phaseCheckers:PhaseChecker[]){
        super();
    }

    public IsOver(characters: Character[], phaseNumber: number): boolean {
        for (let i = 0; i < this._phaseCheckers.length; i++) 
        {
            if(this._phaseCheckers[i].IsOver(characters,phaseNumber))
            {
                characters.forEach(c=>c.SetVisible(true));
                return true;
            }
        }
        return false;
    }

}