import { GameStatus } from '../../GameStatus';
import { Character } from '../../../Character/Character';
import { Wolf } from '../../../Character/Job/Wolf';

export class WolfPhaseChecker{
    public IsOver(characters: Character[], phaseNumber: number): boolean {
        const wolfs = characters.filter((c) => (c.Job instanceof Wolf));
        const others = characters.filter((c) => !(c.Job instanceof Wolf));
        if (
            wolfs.filter(w=>!w.IsDead()).length > 0
            && others.filter(w=>!w.IsDead()).length === 0
		) {
            wolfs.forEach(w=>w.SetStatus(GameStatus.Won));
            others.forEach(w=>w.SetStatus(GameStatus.Lost));
			return true;
		}
		return false;
	}
}