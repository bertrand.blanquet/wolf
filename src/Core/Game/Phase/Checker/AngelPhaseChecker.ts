import { PhaseChecker } from './PhaseChecker';
import { GameStatus } from '../../GameStatus';
import { Character } from '../../../Character/Character';
import { Angel } from '../../../Character/Job/Angel';

export class AngelPhaseChecker extends PhaseChecker{
	public IsOver(characters: Character[], phaseNumber: number): boolean {
        const angels = characters.filter((c) => c.Job instanceof Angel);
        if (
			phaseNumber == 1 &&
			angels.length === 1 &&
			angels[0].IsDead()
		) {
            angels[0].SetStatus(GameStatus.Won);
            characters.filter(c=>c !== angels[0]).forEach(c=>c.SetStatus(GameStatus.Lost));
			return true;
		}
		return false;
	}
}
