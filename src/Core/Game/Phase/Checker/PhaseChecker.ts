import { Character } from "../../../Character/Character";

export abstract class PhaseChecker{
    public abstract IsOver(characters:Character[], phaseNumber:number):boolean;
}