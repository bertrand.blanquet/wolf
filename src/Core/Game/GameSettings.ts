import { IJob } from './../Character/Job/IJob';
import { Seer } from '../Character/Job/Seer';
import { Witch } from '../Character/Job/Witch';
import { Angel } from '../Character/Job/Angel';

export class GameSettings{
    public DaytimeDuration:number=30;
    public RedNightDuration:number=30;
    public PurpleNightDuration:number=30;
    public Seer:boolean=false;
    public Witch:boolean=false;
    public Angel:boolean=false;
    public Names:string[];

    public static GetJobs(settings:GameSettings):Array<IJob>{
        let jobs = new Array<IJob>();
        if(settings.Seer){
            jobs.push(new Seer())
        }
        if(settings.Witch){
            jobs.push(new Witch())
        }
        if(settings.Angel){
            jobs.push(new Angel())
        }
		return jobs;
	}
}