import { EnvKind } from './../../Game/Phase/EnvKind';

export interface IJob{
    Title:string;
    GetIcons(phase:EnvKind):string[];
}