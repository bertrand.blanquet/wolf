import { IJob } from './IJob';
import { Citizen } from './Citizen';
import { Wolf } from './Wolf';
import { Seer } from './Seer';
import { Witch } from './Witch';
import { Angel } from './Angel';

export class JobFactory{
    public static Generate(job:string):IJob{
        var j = new Citizen();
		if (job === 'Wolf') {
			j = new Wolf();
		}

		if (job === 'Seer') {
			j = new Seer();
		}

		if (job === 'Witch') {
			j = new Witch();
		}

		if (job === 'Angel') {
			j = new Angel();
        }
        return j;
    }
}