import { EnvKind } from './../../Game/Phase/EnvKind';
import { Job } from "./Job";

export class Witch extends Job{
    public Title:string='Witch';
    public HasSavingPotion:boolean=true;
    public HasKillingPotion:boolean=true;

    GetIcons(phase: EnvKind): string[] {
        const icons = [];

        if(phase === EnvKind.Daytime){
            icons.push('fill-voting-smiley');
        }

        if(phase === EnvKind.PurpleNight){
            if(this.HasSavingPotion){
                icons.push('fill-savingPotion-smiley');
            }

            if(this.HasKillingPotion){
                icons.push('fill-potion-smiley');
            }
        }
        
        this.GetBasicIcons().forEach(i=>{
            icons.push(i);
        });
        return icons;
    }


}