import { Job } from "./Job";
import { EnvKind } from './../../Game/Phase/EnvKind';

export class Wolf extends Job{
    public Title:string='Wolf';

    GetIcons(phase: EnvKind): string[] {
        const icons = [];

        if(phase === EnvKind.Daytime 
            || phase === EnvKind.RedNight)
        {
            icons.push('fill-voting-smiley');
        }
        
        this.GetBasicIcons().forEach(i=>{
            icons.push(i);
        });
        return icons;
    }
}