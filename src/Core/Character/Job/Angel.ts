import { EnvKind } from './../../Game/Phase/EnvKind';
import { Job } from "./Job";

export class Angel extends Job {
    public Title:string='Angel';
    
    GetIcons(phase: EnvKind): string[] {
        const icons = [];

        if(phase === EnvKind.Daytime){
            icons.push('fill-voting-smiley');
        }
        
        this.GetBasicIcons().forEach(i=>{
            icons.push(i);
        });
        return icons;
    }
}