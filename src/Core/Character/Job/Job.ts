import { IJob } from "./IJob";
import { EnvKind } from './../../Game/Phase/EnvKind';

export abstract class Job implements IJob{
    public abstract Title: string;
    abstract GetIcons(phase: EnvKind): string[];
    protected GetBasicIcons():string[]{
        return [
			'fill-default-smiley',
			'fill-wolf-smiley',
			'fill-begging-smiley',
			'fill-death-smiley',
			'fill-danger-smiley',
		];
    }

}