import { Job } from "./Job";
import { EnvKind } from './../../Game/Phase/EnvKind';

export class Seer extends Job{
    public Title:string='Seer';
    public HasBall:boolean=true;
    GetIcons(phase: EnvKind): string[] {
        const icons = [];

        if(phase === EnvKind.Daytime){
            this.HasBall = true;
            icons.push('fill-voting-smiley');
        }

        if(phase === EnvKind.PurpleNight && this.HasBall){
            icons.push('fill-magic-smiley');
        }
        
        this.GetBasicIcons().forEach(i=>{
            icons.push(i);
        });
        return icons;
    }
}