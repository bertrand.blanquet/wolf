import { Job } from "./Job";
import { EnvKind } from './../../Game/Phase/EnvKind';

export class Citizen extends Job{
    public Title:string='Citizen';

    GetIcons(phase: EnvKind): string[] {
        const icons = [];

        if(phase === EnvKind.Daytime){
            icons.push('fill-voting-smiley');
        }
        
        this.GetBasicIcons().forEach(i=>{
            icons.push(i);
        });
        return icons;
    }
}