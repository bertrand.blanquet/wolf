import { Angel } from './Job/Angel';
import { JobFactory } from './Job/JobFactory';
import { Character } from './Character';
import { Profil } from './Profil';
import { Citizen } from './Job/Citizen';
import { Seer } from './Job/Seer';
import { Wolf } from './Job/Wolf';
import { Witch } from './Job/Witch';
import { IJob } from './Job/IJob';
import { GameSettings } from '../Game/GameSettings';


export class CharacterFactory {
	private static Profiles = [
		()=>new Profil('fill-orange-woman', '#B2D38D', 'fill-green-color'),
		()=>new Profil('fill-circle-woman', '#E2DF81', 'fill-yellow-color'),
		()=>new Profil('fill-blond-woman', '#81B5E3', 'fill-blueMarine-color'),
		()=>new Profil('fill-glass', '#D1A5CD', 'fill-purple-color'),
		()=>new Profil('fill-moustache', '#A2D7DE', 'fill-blue-color'),
		()=>new Profil('fill-hat-man', '#CEBBA5', 'fill-brown-color')
	];

	public static GenerateFromJob(job: string): Character[] {
		return [
			Character.Create('Bob', this.Profiles[0](), JobFactory.Generate(job)),
			Character.Create('Alice', this.Profiles[1](), new Citizen()),
			Character.Create('Rachel', this.Profiles[2](), new Seer()),
			Character.Create('Sofia', this.Profiles[3](), new Wolf()),
			Character.Create('David', this.Profiles[4](), new Witch()),
			Character.Create('Kevin', this.Profiles[5](), new Wolf())
		];
	}

	public static GetCount(playerCount:number,modulo:number):number{
		let wolftCount = 0;
		for (let i = 0; i < playerCount; i++) {
			if(i%modulo===0){
				wolftCount++;
			}
		}
		return wolftCount;
	}

	public static Generate(setting: GameSettings): Character[] {
		const characters = new Array<Character>();
		
		setting.Names.forEach(name=>{
			characters.push(Character.Create(name,this.Profiles[setting.Names.indexOf(name)](),new Citizen()));
		});

		this.SetWolf(this.GetCount(setting.Names.length,4), setting.Names, characters); 
		this.SetJobs(this.GetCount(setting.Names.length,4),setting.Names,characters,GameSettings.GetJobs(setting));

		return characters;
	}

	public static GetJobs():Array<IJob>{
		return [
			new Witch(),
			new Seer(),
			new Angel()
		]
	}

	private static PickJob(jobs:Array<IJob>):IJob{
		const index = Math.floor(Math.random() * jobs.length);
		return jobs.splice(index,1)[0];
	}

	private static SetJobs(jobCounts: number, names: string[], characters: Character[],jobs:Array<IJob>) {
		for (let i = 0; i < jobCounts; i++) {
			if(jobs.length === 0){
				return;
			}
			const jobIndex = Math.floor(Math.random() * names.length);
			if (characters[jobIndex].Job instanceof Citizen) {
				characters[jobIndex].Job = this.PickJob(jobs);
			}
			else {
				i--;
			}
		}
	}

	private static SetWolf(wolfCount: number, names: string[], characters: Character[]) {
		for (let i = 0; i < wolfCount; i++) {
			const wolfIndex = Math.floor(Math.random() * names.length);
			if (characters[wolfIndex].Job instanceof Citizen) {
				characters[wolfIndex].Job = new Wolf();
			}
			else {
				i--;
			}
		}
	}
}
