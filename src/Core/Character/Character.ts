import { ConnectionKind } from './../../Utils/Network/ConnectionKind';
import { WolfService } from './../../Services/WolfService';
import { Witch } from './Job/Witch';
import { EnvKind } from './../Game/Phase/EnvKind';
import { GameStatus } from './../Game/GameStatus';
import { LiteEvent } from '../../Utils/EventHandler/LiteEvent';
import { Profil } from './Profil';
import { IJob } from './Job/IJob';
import { CharacterMessage } from './CharacterMessage';
import { Seer } from './Job/Seer';

export class Character {
	public Name: string;
	public Profil:Profil;

	private _connection: ConnectionKind;
	public GetConnection(): ConnectionKind {
		return this._connection;
	}

	public SetConnection(value: ConnectionKind): void {
		this._connection = value;
		this.Changed.Invoke(this, 'Connection');
    }

	private _latency: string = '';
	public GetLatency(): string {
		return this._latency;
	}
	public SetLatency(value: string): void {
		this._latency = value;
		this.Changed.Invoke(this, 'Latency');
    }

	private _isMayor: boolean = false;
	public IsMayor(): boolean {
		return this._isMayor;
	}
	public SetMayor(value: boolean): void {
		this._isMayor = value;
		this.Changed.Invoke(this, 'IsMayor');
    }
    private _isCalled: boolean = false;
	public IsCalled(): boolean {
		return this._isCalled;
	}
	public SetCalled(value: boolean): void {
		this._isCalled = value;
		this.Changed.Invoke(this, 'IsCalled');
	}
	private _isDead: boolean = false;
	public IsDead(): boolean {
		return this._isDead;
	}
	private _isTarget: boolean;
	public SetTarget(value: boolean): void {
		this._isTarget = value;
		this.Changed.Invoke(this, 'IsTarget');
	}
	public IsTarget(): boolean {
		return this._isTarget;
	}
	public SetDead(value: boolean): void {
		this._isDead = value;
		if(this._isDead){
			this._isVisible = true;
			this.Profil.SetDead();
			this.Changed.Invoke(this, 'IsDead');
		}
		else
		{
			this.Profil.ResetPicture();
			this._isVisible = false;
		}
	}

	private _status: GameStatus=GameStatus.Pending;
	public SetStatus(value: GameStatus): void {
		this._status = value;
		this.Changed.Invoke(this, 'Status');
	}
	public GetStatus(): GameStatus {
		return this._status;
	}

	public Job: IJob;
	private _isVisible: boolean = false;
	public IsVisible(): boolean {
		return this._isVisible;
	}
	public SetVisible(value: boolean): void {
		this._isVisible = value;
		this.Changed.Invoke(this, 'IsVisible');
	}
	private Vote: number = 0;
	public GetVote(): number {
		return this.Vote;
	}
	public HasMayorVote:boolean;
	public DroppingVote: LiteEvent<Character> = new LiteEvent<Character>();
	public Changed: LiteEvent<string> = new LiteEvent<string>();
	public IconsChanged: LiteEvent<string[]> = new LiteEvent<string[]>();
	public ReceivedMessage: LiteEvent<CharacterMessage> = new LiteEvent<CharacterMessage>();

	public UpdateIcons(kind: EnvKind) {
		if (this.IsDead()) {
			if (this.IsMayor()) {
				this.IconsChanged.Invoke(this, [ 'fill-mayor-smiley' ]);
			} else {
				this.IconsChanged.Invoke(this, []);
			}
		} else {
			const icons = this.Job.GetIcons(kind);
			this.IconsChanged.Invoke(this, icons);
		}
	}

	private _callBack: any;
	constructor() {
		this._callBack = this.OnDropping.bind(this);
	}

	public static Create(name: string, profil:Profil, job: IJob): Character {
		var c = new Character();
		c.Name = name;
		c.Profil = profil;
		c.Job = job;
		return c;
	}


	public ReceiveMessage(message: CharacterMessage) {
		if (message.IsVote()) {
			message.Sender.DroppingVote.On(this._callBack);
			this.Vote += 1;
			if(message.Sender.IsMayor()){
				this.HasMayorVote = true;
			}
			this.Changed.Invoke(this, 'message');
		}

		if (message.IsMagicBall()
			&& WolfService.Context.Player.Job instanceof Seer) {
			this.SetVisible(true);
			let seer = message.Sender.Job as Seer;
			seer.HasBall = false;
			message.Sender.IconsChanged.Invoke(this,seer.GetIcons(EnvKind.PurpleNight));
		}

		if (message.IsMayor()) {
            if(message.Sender.IsMayor()){
                message.Sender.SetMayor(false);
                message.Sender.IconsChanged.Invoke(this,[]);
            }
			this.SetMayor(true);
		}

		if (message.IsRedPotion()) {
			this.SetTarget(true);
			let witch = message.Sender.Job as Witch;
			witch.HasKillingPotion = false;
			message.Sender.IconsChanged.Invoke(this,witch.GetIcons(EnvKind.PurpleNight));
		}

		if (message.IsGreenPotion()) {
			this.SetTarget(false);
			let witch = message.Sender.Job as Witch;
			witch.HasSavingPotion = false;
			message.Sender.IconsChanged.Invoke(this,witch.GetIcons(EnvKind.PurpleNight));
		}
		
		this.ReceivedMessage.Invoke(this, message);
	}

	public Clear(): void {
		this.Changed.Invoke(this, 'vote');
		this.DroppingVote.Clear();
		this.Vote = 0;
	}

	private OnDropping(sender: any, data: Character): void {
		data.DroppingVote.Off(this._callBack);
		this.Vote -= 1;
		if(data.IsMayor()){
			this.HasMayorVote = false;
		}
		this.Changed.Invoke(this, 'vote');
	}
}
