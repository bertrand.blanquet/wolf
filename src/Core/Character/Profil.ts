export class Profil
{
	private _originalPicture:string;
	public Color: string;
	public Picture: string;
	public Background: string;

    constructor(picture: string, color: string, background: string){
		this._originalPicture = picture;
		this.Background = background;
		this.Picture = picture;
		this.Color = color;
    }

    public SetDead():void{
		this.Picture = 'fill-death bouncing-down-up-animation';
	}
	
	public ResetPicture():void{
		this.Picture = this._originalPicture;
	}
}