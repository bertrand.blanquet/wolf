import { Character } from './Character';
import { Profil } from './Profil';
export class CharacterMessage{
    public Sender:Character;
    public Text:string;

    constructor(){
        this.Sender = new Character();
        this.Sender.Profil = new Profil('','#E2E2E2','fill-white-color');
    }

    public IsVote():boolean{
        return this.Text === 'fill-voting-smiley';
    }

    public IsMagicBall():boolean{
        return this.Text === 'fill-magic-smiley';
    }

    public IsMayor():boolean{
        return this.Text === 'fill-mayor-smiley';
    }

    public IsRedPotion():boolean{
        return this.Text === 'fill-potion-smiley';
    }

    public IsGreenPotion():boolean{
        return this.Text === 'fill-savingPotion-smiley';
    }

    public IsTarget():boolean{
        return this.Text === 'fill-target';
    }

    public IsDropping():boolean{
        return this.Text === 'dropping';
    }

    public IsAction():boolean{
        return this.IsGreenPotion() 
        || this.IsRedPotion()
        || this.IsVote();
    }
}